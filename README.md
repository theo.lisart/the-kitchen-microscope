# The Kitchen Microscope (mkI, unclean share)

![https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/theo.lisart/](images/showcase.jpg)

In the context of ULB Fabzero course, this project is my submission for the jury. The Kitchen Microscope (working title, at the moment if it is carried out), is a 3D printed microscope for spherical lenses where the optics can be made at home. Those available files are the final design in the context of the course, but will be carried out as many, many improvements can and should be made.

## Finding or making lenses
The ball lenses required for this project can either be made following the [instructions](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/theo.lisart/#Tutorials/kitchen_lenses/), or bought as small acrylic balls on the internet. The purpose of this project however, is being able to build a relatively useful microscope with the least possible industrial advantages (except if we are really playing this game, commonly used and collectively owned fablab tools in a collapsed world with 100% recycled PLA). You can of course always buy your lenses, I cannot stop you, it is probable I am not in the same room to beat you up while you are browsing Ebay.

## Printing the Microscope
Using standard PLA, it is encouraged to use relatively good precision, but not required except for the lens holder. If you cannot find the perfect fit for your printer, you will have to mess with the parameters in .scad files. The only part that will not properly scale is the clamp design. If you want to mess with those parameters you will either have to wait, or fix it yourself. It's not too much work, but I'll have this time only in a few weeks. This is not a very sensitive print, thanks to the flexible nature of PLA.

* Printing the lens holders without brim, preferably on glass with the writing on the bottom of the heat bed.
* Printing the clamp on its side with support materials (if Prusaslicer and not CUDA might be a good idea to lower support spacing, might work well nonetheless) and brim.
* Printing the sampler holder with no support nor brim

Do not be afraid to force a little the lenses in their holders. By their geometry and smallness cracks don't really have room to spread (unless you are very strong), in that case, very nice.

## Laser cutting the slides
A file is available in the slides_cutter. You can play with it too. On color engrave everything except the black lines where you need to vector cut. The idea is to use something that is a recycled part. Since covid all Fablabs have a lot of face-shields leftovers and this is what I used. The surface is rather small and should work perfectly on very little material. If you have tools to cut glass, this should also work fine. Attention that you might have to do a few tries. The fit must be tight, but not too much such as you can adjust the depth of the sample holder if you want to explore big droplets on the front-facing camera without touching the lens.

## Additional notes
Explanatory poster available on the documentation website, as well as the entire story of this project. Feel free to contribute.
I plan on maintaining this project, and improve upon what I build. Quite a lot of ideas have been explored but the minimalist nature of this is quite elegant. We need more freely available resilient designs either in science or in everyday life.

Théo 
