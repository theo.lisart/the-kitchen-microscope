/*
Théo Lisart  -- FabZero 2021 --

Clamp design 
----------------

License : GPL3
*/


flat_size_length = 55; 
clamp_thickness = 23; 
clamp_opening = 17; 
clamp_girth = 2;

non_linear_length = 80;
connection_amplitude = 6; 
holder_size_clip = 16;

h = 150;
frequency = 4;
phase = 250;

module clamp(flat_size_length, clamp_thickness, clamp_girth, clamp_opening, connection_amplitude, holder_size_clip, non_linear_length, frequency, phase, h) {

difference(){
        union(){
            
            // Straight path 
                cube([flat_size_length, clamp_thickness, clamp_girth], center=true);

            // Camp opening 
                translate(v = [flat_size_length/2 + clamp_girth/2, 0, clamp_opening/2 - clamp_girth/2])
                cube([clamp_girth, clamp_thickness, clamp_opening], center=true);

            // Sine path 
            step = non_linear_length/h; 
            
            translate(v = [-13.4, 0, -16]) 
            union(){
                difference(){
                    // First column of sines
                    for(i = [0 : step : non_linear_length]){
                        
                        translate(v = [i*step, 0, connection_amplitude*sin(i*step*frequency + phase)/2])
                        cube([step, clamp_thickness, connection_amplitude*sin(i*step*frequency + phase) + flat_size_length], center = true);
                    }
                    
                    // Second column of sines
                    
                    for(i = [0 : step : non_linear_length]){
                        
                        translate(v = [i*step, 0, connection_amplitude*sin(i*step*frequency + phase)/2 - clamp_girth - i*0.022 + 1])
                        cube([step + 0.1, clamp_thickness + 0.1, connection_amplitude*sin(i*step*frequency + phase) + flat_size_length], center = true);
                    }
                }
            }
          translate(v = [-12, 0, -1])  
          cube([holder_size_clip + 2, holder_size_clip + 7, clamp_girth + 1], center=true);
         } 
         
      translate(v = [-12, 0, 0])
      cube([holder_size_clip, holder_size_clip, clamp_girth + 5], center=true);
     }
     

}

clamp(flat_size_length, clamp_thickness, clamp_girth, clamp_opening, connection_amplitude, holder_size_clip,non_linear_length, frequency, phase, h);