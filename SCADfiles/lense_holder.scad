/*
Théo Lisart  -- FabZero 2021 --

Lense holder squared version with text
[UNCLEAN]
----------------

License : GPL3
*/


// Includes

$fn = 90;
//

lense_radius = 0.4;   // in mm
holder_size = 15;     // in mm, should be tested for standardisation
 
male_rail_position = 0;   // the 0 position is the upper contact with the rest of the microscope 
male_rail_size = 1;       // length/size of the extra material for rail  
male_rail_size_extra = 0;
font_size = holder_size/4.5;
delta_text = -0.6;
text_extrude = lense_radius;


module disk_lense_holder(lense_radius, holder_size, epsilon_thickness, male_rail_position, male_rail_size,male_rail_size_extra, delta_text ,delta_print){
    
    d_lense = 2*lense_radius;
    e = epsilon_thickness;
    
    difference(){
        difference(){
            union(){
                cube(size = [holder_size, holder_size, d_lense], center = true);
                translate(v = [0, 0, lense_radius - male_rail_size/2])
                    cube(size = [holder_size + male_rail_size_extra, holder_size + male_rail_size_extra, male_rail_size], center = true);
            }
            cylinder(h = d_lense + d_lense + epsilon_thickness + 2, r = lense_radius + delta_print, center = true);
        }
        
        // Adding information text in the lense holder
        string = concat(str(lense_radius*2), "mm");
        
        translate(v = [-font_size - 3.5, -holder_size/2 + 1, lense_radius/2])
        linear_extrude(text_extrude)
            text(string[0], font="Liberation Sans", size = font_size);
            
        translate(v = [-font_size + font_size + delta_text, -holder_size/2 + 0.5, lense_radius/2])

        linear_extrude(text_extrude)
            text(string[1], font="Liberation Sans", size = font_size);
    }

}

epsilon_thickness = 0;
delta_print = 0;  // managing tolerances relative to the male railing system
disk_lense_holder(lense_radius, holder_size, epsilon_thickness, male_rail_position, male_rail_size,male_rail_size_extra, delta_text ,delta_print);