/*
Théo Lisart  -- FabZero 2021 --

- Clip design to connect to the the clamp system 
- Foot to hold the system
- Sampler holder


Uncomment what you need at the bottom of this file. 
[UNCLEAN]
----------------

License : GPL3
*/


clamp_girth = 3;
non_linear_length = 80;
connection_amplitude = 5; 
holder_size_clip = 16;

module feet(clamp_girth, connection_amplitude, holder_size_clip) {
    
    difference(){
        union(){
            difference(){
                cube([holder_size_clip + 8, holder_size_clip + 12, clamp_girth + 4], center=true);
                cube([holder_size_clip + 2.8, holder_size_clip + 6.8, clamp_girth + 12.5], center=true);
                }
        }
        translate(v = [-5, 15, 0])
        rotate([0, 40, 0])
        cube([4, 10, 4], center=true);

        translate(v = [5, 15, 0])
        rotate([0, 40, 0])
        cube([4, 10, 4], center=true);
        
        translate(v = [-5, -6.8, 3.7])
        cube([35, 2.3, clamp_girth + 9],center = true);

        translate(v = [-5, 6.8, 3.7])
        cube([35, 2.3, clamp_girth + 9],center = true);

    }
}




module bar(){
    
    cube([3.5, 4, 3.5], center=true);
    
    translate(v = [30/2 - 3.5/2, 4/2 + 3.5/2, 0])
    cube([30, 3.5, 3.5], center=true);

}


module slider(){
        translate(v = [0, -6.8, 1.5])
        cube([32, 1.4, 2],center = true);

        translate(v = [0, 6.8, 1.5])
        cube([32, 1.4, 2],center = true);
    
    
        translate(v = [-6.8, 0, 1.5])
        cube([1.4, 14, 2],center = true);

        translate(v = [6.8, 0, 1.5])
        cube([1.4, 14, 2],center = true);
    
    
        // Holder fingers
        translate(v = [-15.1, 0, 1.5])
        cube([1.4, 16.5, 2],center = true);

        translate(v = [15.1, 0, 1.5])
        cube([1.4, 16.5, 2],center = true);

}

feet(clamp_girth, connection_amplitude, holder_size_clip);
slider();
//bar();